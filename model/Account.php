<?php
/**
 * Account model
 *
 * Describes the account model
 *
 * @author Sergey Zamyatin <zsa_spb@mail.ru>
 * @version 1.0
 */

namespace App\Model;

class Account extends Model
{
    public $id;
    public $name;
    public $billNumber;
    public $balance;

    public function initialize()
    {
        $this->hasMany(
            'id',
            Transaction::class,
            'accountFrom'
        );

        $this->hasMany(
            'id',
            Transaction::class,
            'accountTo'
        );
    }

    /**
     * The method covered by ORM is not implemented within the task
     *
     * @return Transaction[] Array of transaction objects
     */
    public function getTransaction($params)
    {

    }
}
