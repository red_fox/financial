<?php
/**
 * Base class of model
 *
 * @author Sergey Zamyatin <zsa_spb@mail.ru>
 * @version 1.0
 */

namespace App\Model;

class Model
{
    /**
     * The implementation of interaction with the database is not implemented
     * within the task
     *
     * @return array
     */
    public static function find()
    {

    }

    /**
     *
     * @return static
     */
    public static function findFirst($id)
    {

    }

    /**
     *
     * @return boolean
     */
    public function store()
    {

    }
}
