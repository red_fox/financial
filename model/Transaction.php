<?php
/**
 * Transaction model
 *
 * Describes the transaction model
 *
 * @author Sergey Zamyatin <zsa_spb@mail.ru>
 * @version 1.0
 */

namespace App\Model;

class Transaction extends Model
{
    // Transaction types
    const TYPE_DEPOSIT = 'deposit';
    const TYPE_WITHDRAWAL = 'withdrawal';
    const TYPE_TRANSFER = 'transfer';

    /**
     * @var integer
     */
    public $id;

    /**
     * @var integer
     */
    public $accountFrom;

    /**
     * @var integer
     */
    public $accountTo;

    /**
     * @var string
     */
    public $type;

    /**
     * @var integer
     */
    public $amount;

    /**
     * @var string
     */
    public $comment;

    /**
     * @var string
     */
    public $date;

    public function initialize()
    {
        $this->belongsTo(
            'accountFrom',
            Account::class,
            'id'
        );

        $this->belongsTo(
            'accountTo',
            Account::class,
            'id'
        );
    }
}
