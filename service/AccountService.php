<?php
/**
 * Account managing service
 *
 * Service for managing the financial operations of an account
 *
 * @author Sergey Zamyatin <zsa_spb@mail.ru>
 * @version 1.0
 */

namespace App\Service;

use App\TransactionDB;
use App\Model\Transaction;
use App\Model\Account;

class AccountService extends Service
{
    /**
     * Return all accounts in system
     *
     * @return array Array of account objects
     */
    public function getAllAccounts()
    {
        return Account::find();
    }

    /**
     * Get account balance
     *
     * @param integer $accountId The account identificator
     * @return integer Balance of account
     */
    public function getBalance($accountId)
    {
        $account = Account::findFirst($accountId);

        return $account->balance;
    }

    /**
     * Perform an operation
     *
     * @param integer $accountId The account identificator
     * @param string $type Type of operation
     * @param string $comment Transaction comment
     * @param int $amount Transaction amount
     * @param integer $responseAccountId Response account id
     * @return boolean Operation result
     */
    public function performOperation($accountId, $type, $comment, $amount, $responseAccountId = 0)
    {
        TransactionDB::start();

        $transaction          = new Transaction();
        $transaction->type    = $type;
        $transaction->amount  = $amount;
        $transaction->comment = $comment;
        $transaction->date    = date('Y-m-d H:i:s');

        $accountOperation = Account::findFirst($accountId);

        switch ($type) {

            case Transaction::TYPE_DEPOSIT:
                $accountOperation->balance += $amount;

                $transaction->link('accountTo', $accountOperation);
                break;

            case Transaction::TYPE_WITHDRAWAL:
                $accountOperation->balance -= $amount;

                $transaction->link('accountFrom', $accountOperation);
                break;

            case Transaction::TYPE_TRANSFER:
                // Withdraw from current account
                $accountOperation->balance -= $amount;

                // Deposit to another account
                $transferAccount = Account::findFirst($responseAccountId);
                $transferAccount->balance += $amount;

                $transaction->link('accountFrom', $accountOperation);
                $transaction->link('accountTo', $transferAccount);

                if (!$transferAccount->store() === true) {
                    TransactionDB::rollback();
                    return false;
                }
                break;
            default:
                TransactionDB::rollback();
                return false;
        }

        if (!$accountOperation->store() || !$transaction->store()) {
            TransactionDB::rollback();
            return false;
        }

        TransactionDB::commit();
        return true;
    }

    /**
     * Get all account transactions
     *
     * @param integer $accountId The account identificator
     * @param string $sortParams Sort option
     * @return Transaction[] Array of transaction objects
     */
    public function getAllAccountTransactions($accountId, $sortParams)
    {
        $account = Account::findFirst($accountId);

        return $account->getTransaction([
            'query' => 'accountFrom = :accountId: OR accountTo = :accountId:',
            'bind' => [
                'accountId' => $accountId,
            ],
            'order' => $sortParams,
        ]);
    }
}
